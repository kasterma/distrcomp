package net.kasterma.paxos;

import java.util.Queue;

// All the actors in this simulation need to be uniquely identifiable

public abstract class Actor {
	static int nextId = 0;
	int id;
	Queue<Message> mq;
	
	Actor() {
		id = getNextId();
	}
	
	static synchronized int getNextId() {
		return nextId++;
	}
	
	abstract String getId();
	abstract void act();

	// in this method give no hint of if message was delivered
	// later we can have some messages get lost by dropping them here
	void recv(Message m) {
		mq.add(m);
	}
}
