package net.kasterma.paxos;

public class Learner extends Actor {
	boolean learned;
	int value;
	
	Learner() {
		learned = false;
	}
	
	boolean learn(int val) {
		value = val;
		learned = true;
		System.out.printf("Learner %d learned: %d\n", id, val);
		return true;
	}
	
	String getId() {
		return "Learner:" + id;
	}

	@Override
	void act() {	
	}
}
