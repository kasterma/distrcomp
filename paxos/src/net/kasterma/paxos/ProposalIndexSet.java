package net.kasterma.paxos;

public class ProposalIndexSet {
	int next; // next value to use
	int step; // step between values chosen
	
	ProposalIndexSet(int first, int step) {
		next = first;
		this.step = step;
	}
	
	public int next() {
		int rv = next;
		next += step;
		return rv;
	}
}
