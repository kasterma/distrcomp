package net.kasterma.paxos;

public class Proposer extends Actor {
   ProposalIndexSet pis;
   Acceptor[] accs;
   
   Proposer(ProposalIndexSet pis, Acceptor[] accs) {
	   this.pis = pis;   // the proposer needs to be told which indices it can use
	   this.accs = accs; // the proposer needs to find out which acceptors there are, here just tell it
   }

	@Override
	String getId() {
		return "Proposer:" + id;
	}

	@Override
	void act() {
		// pick index number
		// ask for promise
		// propose
	}
}
