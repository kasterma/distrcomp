package net.kasterma.paxos;

public class Runner {
	static int numProposers = 3;
	static int numAcceptors = 3;
	static int numLearners = 3;
	static Acceptor[] accs;
	static Proposer[] props;
	static Learner[] learns;
	
	public static void main(String[] args) {
		accs = new Acceptor[numAcceptors];
		for(int accepidx = 0; accepidx < numAcceptors; accepidx++) {
			accs[accepidx] = new Acceptor();
		}

		props = new Proposer[numProposers];
		for(int propidx = 0; propidx < numProposers; propidx++) {
			props[propidx] = new Proposer(new ProposalIndexSet(propidx, numProposers), accs);
		}
		
		learns = new Learner[numLearners];
		for(int learnidx = 0; learnidx < numLearners; learnidx++) {
			learns[learnidx] = new Learner();
		}
		
		for(Proposer p : props) {
			System.out.println(p.getId());
		}
	}

}
