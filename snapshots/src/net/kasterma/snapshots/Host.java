package net.kasterma.snapshots;

import java.util.ArrayList;
import java.util.Random;

// Class that represents a host

public class Host implements Runnable , Snapshotable<Integer> {
	int id;
	int value;
	MessageQueue mq;
	Random rand = new Random();
	
	Boolean sending = true;
	Boolean receiving = true;
	
	Host(int id, int value, MessageQueue mq, int nbds) {
		this.id = id;
		this.value = value;
		this.mq = mq;
		this.nbds = nbds;
	}
	
	public void send() {
		int message = rand.nextInt(value);
		value = value - message;
		int toHost = rand.nextInt(mq.getCount());
		mq.send(id, toHost, message);
	}
	
	public void recv() {
		for (int i = 0; i < mq.getCount(); i++) {
			int message = mq.get(i, id);
			if (isSavetoken(message)) {
				if (savingMessages) {
					if (queueDone != null)
						queueDone[i] = true;
				} else {
					savingMessages = true;
					saveState();
					queueDone = new Boolean[nbds];
					for(int nbd = 0; nbd < nbds; nbd++) {
						queueDone[nbd] = false;
					}
					queueDone[i] = true;
					for (int nbd = 0; nbd < nbds; nbd++) {
						mq.send(id, nbd, message); // send all nbds a saving message
					}
				}
			} else {
				value += message;
				if (savingMessages) {
					saveMessage(i, message);
				}
			}
		}
	}
	
	public int getValue() {
		return value;
	}
	
	public int id() {
		return id;
	}
	
	public void stopSending() {
		sending = false;
	}
	
	public void stopReceiving() {
		receiving = false;
	}

	@Override
	public void run() {
		while(sending || receiving) {
			if (sending)
				send();
			if (receiving)
				recv();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	int savedState;
	boolean savingMessages = false;
	int nbds;
	ArrayList<Integer>[] queues;
	Boolean[] queueDone;
	
	@Override
	public void saveState() {
		savedState = value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveMessage(int fromHost, Integer m) {
		if (queues == null) {
			queues = (ArrayList<Integer>[]) new ArrayList[nbds];
			for(int i = 0; i < nbds; i++) {
				queues[i] = new ArrayList<Integer>();
			}
		}
		if (!queueDone[fromHost])
			queues[fromHost].add(m);
	}
	

	@Override
	public void queueDone(int host) {
		queueDone[host] = true;
	}

	@Override
	public Boolean isSavetoken(Integer m) {
		return (m < 0);
	}

	@Override
	public Boolean snapshotDone() {
		if(queueDone == null)
			return false;
		for (Boolean done : queueDone) {
			if (!done) return false;
		}
		return true;
	}

	@Override
	public String getSnapshot() {
		StringBuilder rv = new StringBuilder();
		rv.append("Host: ");
		rv.append(id);
		rv.append(" state: ");
		rv.append(savedState);
		for(int i = 0 ; i < nbds; i++) {
			rv.append(" messages from ");
			rv.append(i);
			rv.append(" : ");
			for(Integer m : queues[i]) {
				rv.append(m);
				rv.append(" ");
			}
		}
		return rv.toString();
	}
}
