package net.kasterma.snapshots;

import java.util.ArrayDeque;
import java.util.NoSuchElementException;
import java.util.Queue;

// Class that maintains all message queues, where for instance we can
// add some delays to passing messages on.

public class MessageQueue {
	Queue<Integer> queues[][];
	int numHosts;
	
	@SuppressWarnings("unchecked")
	MessageQueue(int numHosts) {
		this.numHosts = numHosts;
		queues = (ArrayDeque<Integer>[][]) new ArrayDeque[numHosts][numHosts];
		for(int i = 0; i < numHosts; i++) {
			for (int j = 0; j < numHosts; j++) {
				queues[i][j] = new ArrayDeque<Integer>();
			}
		}
	}
	
	public void send(int fromHost, int toHost, int message) {
		queues[fromHost][toHost].add(message);
	}
	
	public Integer get(int fromHost, int toHost) {
		try{
			return queues[fromHost][toHost].remove();
		} catch (NoSuchElementException e) {
			return 0;
		}
	}
	
	public int getCount() {
		return numHosts;
	}
}
