package net.kasterma.snapshots;

public class Runner {
	static Host hosts[];
	
	public static void main(String[] args) {
		int numHosts = 5;
		MessageQueue mq = new MessageQueue(numHosts);
		hosts = new Host[numHosts];
		for(int i = 0; i < numHosts; i++) {
			hosts[i] = new Host(i, 5, mq, numHosts);
			new Thread(hosts[i]).start();
		}
		for(int i = 0; i < 10; i++) {
			showHosts();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		mq.send(0, 0, -1); // save a snapshot
		for(int i = 0; i < 5; i++) {
			showHosts();
			Boolean done = true;
			for (int h = 0; h < numHosts; h++) {
				if (!hosts[h].snapshotDone())
					done = false;
			}
			if (done){
				for (int h = 0; h < numHosts; h++) {
					System.out.println(hosts[h].getSnapshot());
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for(Host host : hosts) {
			host.stopSending();
		}
		System.out.println("stop sending");
		for(int i = 0; i < 2; i++) {
			showHosts();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for(Host host : hosts) {
			host.stopReceiving();
		}
	}
	
	static void showHosts() {
		for (Host host : hosts) {
			System.out.printf("%d ", host.getValue());
		}
		System.out.println();
	}
}
