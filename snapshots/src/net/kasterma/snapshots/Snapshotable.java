package net.kasterma.snapshots;

public interface Snapshotable<MessageType> {
	public void saveState();
	public void saveMessage(int fromHost, MessageType m);
	public void queueDone(int host);
	public Boolean isSavetoken(MessageType m);
	public Boolean snapshotDone();
	public String getSnapshot();
}
